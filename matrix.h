#pragma once

#include <vector>
#include <stdio.h>

template<class T>
class Matrix {
 public:
    Matrix(int size = 0, T values[]={0});

    // доступ к элементам по индексам
    const T &item(int row, int column) const;
    T &item(int row, int column);
    const T &operator()(int row, int column) const;
    T &operator()(int row, int column);

    // получение "реальных" индексов
    // e.g. если была удалена строка 1 rowIndex(1) == 2
    // ?необходимо для алгоритма Литтла
    int rowIndex(int row) const;
    int columnIndex(int column) const;

    // размерность матрицы
    int size() const;
    // вывод на экран
    void print() const;
    void printEndl() const;

    // удаление строки и столбца по индексам
    void removeRowColumn(int row, int column);



 private:
    // инициализация "реальных" номеров строк и столбцов
    void initRowsColumns();

    std::vector<std::vector<T>> _items;
    // "реальные" номера строк
    std::vector<int> _rows;
    // "реальные" номера столбцов
    std::vector<int> _columns;
};

template<class T>
inline Matrix<T>::Matrix(int size, T values)
    :_items(size, std::vector<T>(size, values)), _rows(size), _columns(size) {
    initRowsColumns();
}

template<class T>
inline const T & Matrix<T>::item(int row, int column) const {
    return _items[row][column];
}

template<class T>
inline T & Matrix<T>::item(int row, int column) {
    return const_cast<T&>(static_cast<const Matrix<T>&>(*this).item(row, column));
}

template<class T>
inline const T & Matrix<T>::operator()(int row, int column) const {
    return item(row, column);
}

template<class T>
inline T & Matrix<T>::operator()(int row, int column) {
    return item(row, column);
}

template<class T>
inline int Matrix<T>::rowIndex(int row) const {
    return _rows[row];
}

template<class T>
inline int Matrix<T>::columnIndex(int column) const {
    return _columns[column];
}

template<class T>
inline void Matrix<T>::removeRowColumn(int row, int column) {

    _rows.erase(_rows.begin() + row);
    _columns.erase(_columns.begin() + column);

    _items.erase(_items.begin() + row);

    for (int i = 0; i < _items.size(); i++)
        _items[i].erase(_items[i].begin() + column);
}

template<class T>
inline int Matrix<T>::size() const {
    return _items.size();
}

template<class T>
inline void Matrix<T>::print() const {
    printf("         ");
    for (auto iter = _columns.cbegin(); iter != _columns.cend(); ++iter)
        printf("%8u ", *iter);
    puts("");
    for (int i = 0; i < _items.size(); i++) {
        printf("%8u ", _rows[i]);
        for (int j = 0; j < _items.size(); j++) {
            printf("%8.2f ", _items[i][j]);
        }
        puts("");
    }
}

template<class T>
inline void Matrix<T>::printEndl() const {
    print();
    puts("");
}

template<class T>
inline void Matrix<T>::initRowsColumns() {
    for (int i = 0; i < _rows.size(); i++)
        _rows[i] = _columns[i] = i;
}